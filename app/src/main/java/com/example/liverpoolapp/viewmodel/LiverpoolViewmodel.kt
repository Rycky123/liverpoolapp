package com.example.liverpoolapp.viewmodel

import androidx.lifecycle.*
import com.example.liverpoolapp.model.EnumClass
import com.example.liverpoolapp.model.PlpResults
import com.example.liverpoolapp.model.damain.InteractorProducst
import kotlinx.coroutines.launch

class LiverpoolViewmodel(): ViewModel() {


    val interactor = InteractorProducst()
    var LiveDataProductsList = MutableLiveData<PlpResults>()
    var LiveDataEst = MutableLiveData<EnumClass>()

    fun ObserverEnum(): LiveData<EnumClass>{
        return LiveDataEst
    }

    fun ObserverProductList(): LiveData<PlpResults>{
        return LiveDataProductsList
    }



    fun showProducts(term:String, pag:String){
        viewModelScope.launch {
            var result = interactor.dataProducts(term, pag)
            if (result.records.isNotEmpty()){
                LiveDataEst.postValue(EnumClass.SUCCESS)
                LiveDataProductsList.postValue(result)

            }else{
                LiveDataEst.postValue(EnumClass.FAILURE)
            }
        }
    }

}