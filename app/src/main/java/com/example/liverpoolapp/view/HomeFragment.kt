package com.example.liverpoolapp.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.liverpoolapp.R
import com.example.liverpoolapp.databinding.FragmentHomeBinding
import com.example.liverpoolapp.model.EnumClass
import com.example.liverpoolapp.model.Record
import com.example.liverpoolapp.model.adapters.ProductsAdapter
import com.example.liverpoolapp.viewmodel.LiverpoolViewmodel


class HomeFragment : Fragment(), SearchView.OnQueryTextListener {

    lateinit var binding: FragmentHomeBinding
    val mvvm: LiverpoolViewmodel by viewModels()
    var adapterProducts = ProductsAdapter()
    var list = mutableListOf<Record>()
    var listHistoy = ArrayList<Record>()
    var number = ""
    var pag = 1
    var query = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mvvm.showProducts("Otras Categorias", "1")

        initRecycler()

        getDataProducts()
        binding.searchView.setOnQueryTextListener(this)
    }

    fun getDataProducts(){
        mvvm.ObserverProductList().observe(viewLifecycleOwner, Observer { prod ->

            prod.records.forEach {
                list.add(it)
                if (!listHistoy.contains(it)){
                    listHistoy.add(it)
                }

            }
          adapterProducts.setProducts(listHistoy)
            binding.progressBar.visibility = View.INVISIBLE
             number = prod.records.get(prod.records.size-1).productId
        })
    }

    fun getEstado(){
        mvvm.LiveDataEst.observe(viewLifecycleOwner, Observer { est ->
            if (est == EnumClass.SUCCESS){

            }else{
               Toast.makeText(context, "Busqueda terminada", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun initRecycler(){
        binding.recyclerProd.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = adapterProducts
            Pagination()
        }
    }

    fun Pagination(){
        binding.recyclerProd.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                if (list.size>1 && number != list[list.size-2].productId)

                        pag++
                        binding.progressBar.visibility = View.VISIBLE
                            if (!query.isEmpty()){
                                mvvm.showProducts(query, pag.toString())
                            }else{
                                Log.e("Fail", query.toString())
                                mvvm.showProducts("Otras Categorias", pag.toString())
                            }

                }
            }
        })
    }


    override fun onQueryTextSubmit(query: String?): Boolean {
        Log.e("buscadoooooo", query.toString())
        if (query!!.isNotEmpty()){
            this.query = query
            binding.progressBar.visibility = View.VISIBLE
            pag = 1
            listHistoy.clear()
            mvvm.showProducts(query, pag.toString())
        }

        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

}