package com.example.liverpoolapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.liverpoolapp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}