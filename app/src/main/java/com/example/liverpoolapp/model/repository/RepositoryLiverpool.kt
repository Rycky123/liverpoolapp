package com.example.liverpoolapp.model.repository

import com.example.liverpoolapp.model.Products
import com.example.liverpoolapp.model.data.RetrofitInstance
import com.example.liverpoolapp.model.data.apiService
import retrofit2.Response

class RepositoryLiverpool {
    val api = RetrofitInstance


    suspend fun getProducts(term: String, pag: String): Response<Products> {
        return api.ProviderRetrofit().create(apiService::class.java).getProductList(term, pag)
    }
}