package com.example.liverpoolapp.model

data class SortOption(
    val label: String,
    val sortBy: String
)