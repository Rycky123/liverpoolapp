package com.example.liverpoolapp.model.data

import com.example.liverpoolapp.model.Products
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface apiService {


    @GET("appclienteservices/services/v3/plp?")
    suspend fun getProductList(@Query("search-string")term:String, @Query("page-number")pag:String): Response<Products>
}