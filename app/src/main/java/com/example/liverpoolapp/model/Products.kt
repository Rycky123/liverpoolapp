package com.example.liverpoolapp.model

data class Products(
    val pageType: String,
    val plpResults: PlpResults,
    val status: Status
)