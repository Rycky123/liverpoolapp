package com.example.liverpoolapp.model

data class Status(
    val status: String,
    val statusCode: Int
)