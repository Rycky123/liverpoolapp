package com.example.liverpoolapp.model

data class DwPromotionInfo(
    val dWPromoDescription: String,
    val dwToolTipInfo: String
)