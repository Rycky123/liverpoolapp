package com.example.liverpoolapp.model

data class Ancester(
    val categoryId: String,
    val label: String
)