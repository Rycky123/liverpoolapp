package com.example.liverpoolapp.model.damain

import com.example.liverpoolapp.model.PlpResults
import com.example.liverpoolapp.model.repository.RepositoryLiverpool

class InteractorProducst {

    val repository = RepositoryLiverpool()


    suspend fun dataProducts(term:String, pag:String):PlpResults{
        var call = repository.getProducts(term, pag).body()?.plpResults
        return call!!
    }

}