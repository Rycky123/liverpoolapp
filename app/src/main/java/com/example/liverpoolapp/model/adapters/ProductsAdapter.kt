package com.example.liverpoolapp.model.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.liverpoolapp.R
import com.example.liverpoolapp.databinding.ItemProductsBinding
import com.example.liverpoolapp.model.Record

class ProductsAdapter():RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var listProducts = ArrayList<Record>()

    @SuppressLint("NotifyDataSetChanged")
    fun setProducts(products: ArrayList<Record>){
        this.listProducts = products
        notifyDataSetChanged()
    }

    class ViewData(view: View): RecyclerView.ViewHolder(view){
        var binding = ItemProductsBinding.bind(view)
        fun render(lisProd: Record){
            binding.tittleProduct.text = lisProd.productDisplayName
            binding.PrecioActual.text =  lisProd.maximumListPrice.toString()
            binding.precioAnterior.text = lisProd.minimumPromoPrice.toString()
            if (lisProd.variantsColor[0].colorHex.isNotEmpty()){
                binding.cardColor.setBackgroundColor(Color.parseColor(lisProd.variantsColor.get(0).colorHex));
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
     val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_products, parent, false)
        return ViewData(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       val viewHolder: ViewData = holder as ViewData
        var item = listProducts[position]
        viewHolder.render(item)
        Glide.with(holder.itemView).load(listProducts[position].smImage).into(viewHolder.binding.imgProduct)
    }

    override fun getItemCount(): Int {
     return listProducts.size
    }
}