package com.example.liverpoolapp.model

data class Current(
    val categoryId: String,
    val label: String
)